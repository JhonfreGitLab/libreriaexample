package com.jhonfre.libreria.libreria.controller;

import com.jhonfre.libreria.libreria.entity.TipoUsuario;
import com.jhonfre.libreria.libreria.service.TipoUsuarioServiceImplement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tipo_usuario")
public class TipoUsuarioController {
    private TipoUsuarioServiceImplement tipoUsuarioServiceImplement;

    public TipoUsuarioController(TipoUsuarioServiceImplement tipoUsuarioServiceImplement) {
        this.tipoUsuarioServiceImplement = tipoUsuarioServiceImplement;
    }

    @GetMapping("/")
    List<TipoUsuario> get(){
        return tipoUsuarioServiceImplement.get();
    }

    @PostMapping("/agregar_tipo_usuario/")
    ResponseEntity<TipoUsuario> newTipoUser(@RequestBody TipoUsuario tipoUsuario){
        return new ResponseEntity<>(tipoUsuarioServiceImplement.createTipoUsuario(tipoUsuario), HttpStatus.CREATED);
    }

    @DeleteMapping("/eliminar_tipo_usuario/{id}")
    ResponseEntity deleteTipoUsuario(@PathVariable Long id){
        tipoUsuarioServiceImplement.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/actualizar_tipo_usuario/{id}")
    ResponseEntity<TipoUsuario> updateTipoUsuario(@RequestBody TipoUsuario tipoUsuario, @PathVariable Long id){
        return new ResponseEntity<>(tipoUsuarioServiceImplement.updateTipoUsuario(tipoUsuario,id),HttpStatus.OK);
    }
}
