package com.jhonfre.libreria.libreria.controller;

import com.jhonfre.libreria.libreria.entity.Autor;
import com.jhonfre.libreria.libreria.service.AutorServiceImplement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tipo_usuario")
public class AutorController {
    private AutorServiceImplement autorServiceImplement;

    public AutorController(AutorServiceImplement autorServiceImplement) {
        this.autorServiceImplement = autorServiceImplement;
    }

    @GetMapping("/")
    List<Autor> get(){
        return autorServiceImplement.get();
    }

    @PostMapping("/agregar_tipo_usuario/")
    ResponseEntity<Autor> newAutor(@RequestBody Autor autor){
        return new ResponseEntity<>(autorServiceImplement.createAutor(autor), HttpStatus.CREATED);
    }

    @DeleteMapping("/eliminar_tipo_usuario/{id}")
    ResponseEntity deleteAutor(@PathVariable Long id){
        autorServiceImplement.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/actualizar_tipo_usuario/{id}")
    ResponseEntity<Autor> updateAutor(@RequestBody Autor autor, @PathVariable Long id){
        return new ResponseEntity<>(autorServiceImplement.updateAutor(autor,id), HttpStatus.OK);
    }
}
