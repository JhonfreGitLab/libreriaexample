package com.jhonfre.libreria.libreria.controller;

import com.jhonfre.libreria.libreria.entity.Usuario;
import com.jhonfre.libreria.libreria.service.UsuarioServiceImplement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
    private UsuarioServiceImplement usuarioServiceImplement;

    public UsuarioController(UsuarioServiceImplement usuarioServiceImplement) {
        this.usuarioServiceImplement = usuarioServiceImplement;
    }

    @GetMapping("/")
    List<Usuario> get(){
        return usuarioServiceImplement.get();
    }

    @PostMapping("/agregar_usuario")
    ResponseEntity<Usuario> newUser(@RequestParam Long idUsuario, @RequestParam String nombreUsuario,
                                    @RequestParam Long idTipoUser){
        return new ResponseEntity<>(usuarioServiceImplement.createUsuario(idUsuario, nombreUsuario, idTipoUser),
                HttpStatus.CREATED);
    }

    @DeleteMapping("/eliminar_usuario/{id}")
    ResponseEntity deleteUsuario(@PathVariable Long id){
        usuarioServiceImplement.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/actualizar_usuario")
    ResponseEntity<Usuario> updateUsuario(@RequestParam Long idUsuario, @RequestParam String nombreUsuario,
                                          @RequestParam Long idTipoUser){
        return new ResponseEntity<>(usuarioServiceImplement.updateUsuario(idUsuario,nombreUsuario,idTipoUser),HttpStatus.OK);
    }
}