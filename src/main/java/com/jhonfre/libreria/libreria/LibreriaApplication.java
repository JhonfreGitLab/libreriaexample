package com.jhonfre.libreria.libreria;

import com.jhonfre.libreria.libreria.entity.Autor;
import com.jhonfre.libreria.libreria.entity.Libro;
import com.jhonfre.libreria.libreria.entity.TipoUsuario;
import com.jhonfre.libreria.libreria.entity.Usuario;
import com.jhonfre.libreria.libreria.repository.AutorRepository;
import com.jhonfre.libreria.libreria.repository.LibroRepository;
import com.jhonfre.libreria.libreria.repository.TipoUsuarioRepository;
import com.jhonfre.libreria.libreria.repository.UserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class LibreriaApplication implements CommandLineRunner {

	private final Log LOGGER = LogFactory.getLog(LibreriaApplication.class);
	private TipoUsuarioRepository tipoUsuarioRepository;
	private UserRepository userRepository;
	private LibroRepository libroReposiory;
	private AutorRepository autorRepository;

	public static void main(String[] args) {
		SpringApplication.run(LibreriaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		saveAllTipoUsuarios();
		saveAllUsuarios();
		saveAllAutores();
		saveAllLibros();
	}

	public LibreriaApplication(TipoUsuarioRepository tipoUsuarioRepository, UserRepository userRepository, LibroRepository libroReposiory, AutorRepository autorRepository) {
		this.tipoUsuarioRepository = tipoUsuarioRepository;
		this.userRepository = userRepository;
		this.libroReposiory = libroReposiory;
		this.autorRepository = autorRepository;
	}

	public void saveAllTipoUsuarios(){
		TipoUsuario tipoUsuario = new TipoUsuario("Afiliado");
		TipoUsuario tipoUsuario1 = new TipoUsuario("Empleado");
		TipoUsuario tipoUsuario2 = new TipoUsuario("Invitado");
		List<TipoUsuario> tipoUsuarioList = Arrays.asList(tipoUsuario,tipoUsuario1,tipoUsuario2);
		tipoUsuarioList.stream().forEach(tipoUsuarioRepository::save);
	}

	public void saveAllUsuarios(){
		Usuario usuario = new Usuario(1234567890L,"Lorena Vargas",new TipoUsuario(1L));
		Usuario usuario1 = new Usuario(6549873210L,"Juanito Alimaña",new TipoUsuario(1L));
		Usuario usuario2 = new Usuario(9513576540L,"Bertrudiz Carabayo",new TipoUsuario(2L));
		Usuario usuario3 = new Usuario(6549873218L,"Jeison Alarcon",new TipoUsuario(2L));
		Usuario usuario4 = new Usuario(9873219517L,"Juanita Granizado",new TipoUsuario(3L));
		Usuario usuario5 = new Usuario(6549519875L,"Reiner Cespedes",new TipoUsuario(3L));
		Usuario usuario6 = new Usuario(9879513574L,"Valeria Dominguez",new TipoUsuario(1L));
		Usuario usuario7 = new Usuario(9513576542L,"Victoria Secret",new TipoUsuario(2L));
		List<Usuario> tipoUsuarioList = Arrays.asList(usuario,usuario1,usuario2,usuario3,usuario4,
				usuario5,usuario6,usuario7);
		tipoUsuarioList.stream().forEach(userRepository::save);
	}

	public void saveAllAutores() {
		Autor autor = new Autor("Dario Jimenez");
		Autor autor1 = new Autor("Diana Garcia");
		Autor autor2 = new Autor("Raimundo Caribe");
		Autor autor3 = new Autor("Zenaida Alvarez");
		Autor autor4 = new Autor("Flor Arrieta");
		Autor autor5 = new Autor("Gabriel Fernandez");
		Autor autor6 = new Autor("James Muñoz");
		List<Autor> autorList = Arrays.asList(autor,autor1,autor2,autor3,autor4,autor5,autor6);
		autorList.stream().forEach(autorRepository::save);
	}

	public void saveAllLibros(){
		Libro libro = new Libro("El gran matón", new Autor(1L), 150, 5);
		Libro libro1 = new Libro("El diario de Sofia", new Autor(1L), 150, 10);
		Libro libro2 = new Libro("Ojos enmascarados", new Autor(3L), 220, 15);
		Libro libro3 = new Libro("Ali el destripador", new Autor(4L), 80, 40);
		Libro libro4 = new Libro("Libros por doquier", new Autor(5L), 74, 8);
		Libro libro5 = new Libro("Plaza Cesamo", new Autor(6L), 79, 35);
		Libro libro6 = new Libro("Variedades de Cocina", new Autor(6L), 87, 14);
		List<Libro> libroList = Arrays.asList(libro,libro1,libro2,libro3,libro4,libro5,libro6);
		libroList.stream().forEach(libroReposiory::save);
	}
}