package com.jhonfre.libreria.libreria.repository;

import com.jhonfre.libreria.libreria.entity.Autor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutorRepository extends JpaRepository<Autor, Long> {
}
