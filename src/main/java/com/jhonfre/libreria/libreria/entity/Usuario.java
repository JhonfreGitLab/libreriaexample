package com.jhonfre.libreria.libreria.entity;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class Usuario {

    @Id
    @Column(nullable = false, unique = true)
    private Long identification;

    private String nombre;

    @OneToOne
    private TipoUsuario tipoUsuario;

    public Usuario() {
    }

    public Usuario(Long identification) {
        this.identification = identification;
    }

    public Usuario(Long identification, String nombre, TipoUsuario tipoUsuario) {
        this.identification = identification;
        this.nombre = nombre;
        this.tipoUsuario = tipoUsuario;
    }

    public Long getIdentification() {
        return identification;
    }

    public void setIdentification(Long identification) {
        this.identification = identification;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "identification=" + identification +
                ", nombre='" + nombre + '\'' +
                ", tipoUsuario=" + tipoUsuario +
                '}';
    }
}