package com.jhonfre.libreria.libreria.entity;

import javax.persistence.*;

@Entity
@Table(name = "type_user")
public class TipoUsuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    private String description;

    public TipoUsuario() {
    }

    public TipoUsuario(Long id) {
        this.id = id;
    }

    public TipoUsuario(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "TipoUsuario{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
