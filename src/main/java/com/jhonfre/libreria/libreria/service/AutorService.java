package com.jhonfre.libreria.libreria.service;

import com.jhonfre.libreria.libreria.entity.Autor;

import java.util.List;
import java.util.Optional;

public interface AutorService {
    List<Autor> get();
    Autor createAutor(Autor autor);
    void delete(Long id);
    Optional<Autor> findById(Long id);
    Autor updateAutor(Autor autor, Long id);
}
