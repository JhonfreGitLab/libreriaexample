package com.jhonfre.libreria.libreria.service;

import com.jhonfre.libreria.libreria.entity.TipoUsuario;
import com.jhonfre.libreria.libreria.entity.Usuario;
import com.jhonfre.libreria.libreria.repository.TipoUsuarioRepository;
import com.jhonfre.libreria.libreria.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServiceImplement implements UsuarioService{
    private UserRepository userRepository;

    public UsuarioServiceImplement(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<Usuario> get(){
        return userRepository.findAll();
    }

    public Usuario createUsuario(Long idUser, String nombreUsuario, Long idTipoUsuario){
        Usuario usuario = new Usuario();
        usuario.setIdentification(idUser);
        usuario.setNombre(nombreUsuario);
        usuario.setTipoUsuario(new TipoUsuario(idTipoUsuario));
        return userRepository.save(usuario);
    }

    public void delete(Long identificacion){
        userRepository.delete(new Usuario(identificacion));
    }

    public Optional<Usuario> findById(Long id){
        return userRepository.findById(id);
    }

    public Usuario updateUsuario(Long idUser, String nombreUsuario, Long idTipoUsuario){
        return userRepository.findById(idUser)
                .map(
                        user -> {
                            user.setNombre(nombreUsuario);
                            user.setTipoUsuario(new TipoUsuario(idTipoUsuario));
                            return userRepository.save(user);
                        }
                ).get();
    }
}
