package com.jhonfre.libreria.libreria.service;

import com.jhonfre.libreria.libreria.entity.TipoUsuario;

import java.util.List;
import java.util.Optional;

public interface TipoUsuarioService {
    List<TipoUsuario> get();
    TipoUsuario createTipoUsuario(TipoUsuario tipoUsuario);
    void delete(Long id);
    Optional<TipoUsuario> findById(Long id);
    TipoUsuario updateTipoUsuario(TipoUsuario tipoUsuario, Long id);
}
