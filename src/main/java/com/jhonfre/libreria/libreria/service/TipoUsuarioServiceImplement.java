package com.jhonfre.libreria.libreria.service;

import com.jhonfre.libreria.libreria.entity.TipoUsuario;
import com.jhonfre.libreria.libreria.repository.TipoUsuarioRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TipoUsuarioServiceImplement implements TipoUsuarioService{
    private TipoUsuarioRepository tipoUsuarioRepository;

    public TipoUsuarioServiceImplement(TipoUsuarioRepository tipoUsuarioRepository) {
        this.tipoUsuarioRepository = tipoUsuarioRepository;
    }

    public List<TipoUsuario> get(){
        return tipoUsuarioRepository.findAll();
    }

    public TipoUsuario createTipoUsuario(TipoUsuario tipoUsuario){
        return tipoUsuarioRepository.save(tipoUsuario);
    }

    public void delete(Long id){
        tipoUsuarioRepository.delete(new TipoUsuario(id));
    }

    public Optional<TipoUsuario> findById(Long id){
        return tipoUsuarioRepository.findById(id);
    }

    public TipoUsuario updateTipoUsuario(TipoUsuario newTipoUser, Long id){
        return tipoUsuarioRepository.findById(id)
            .map(
                    tipoUser -> {
                        tipoUser.setDescription(newTipoUser.getDescription());
                        return tipoUsuarioRepository.save(tipoUser);
                    }
            ).get();
    }
}
