package com.jhonfre.libreria.libreria.service;

import com.jhonfre.libreria.libreria.entity.Autor;
import com.jhonfre.libreria.libreria.repository.AutorRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AutorServiceImplement implements AutorService{
    private AutorRepository autorRepository;

    public AutorServiceImplement(AutorRepository autorRepository) {
        this.autorRepository = autorRepository;
    }

    public List<Autor> get(){
        return autorRepository.findAll();
    }

    public Autor createAutor(Autor autor){
        return autorRepository.save(autor);
    }

    public void delete(Long id){
        autorRepository.delete(new Autor(id));
    }

    public Optional<Autor> findById(Long id){
        return autorRepository.findById(id);
    }

    public Autor updateAutor(Autor newAutor, Long id){
        return autorRepository.findById(id)
                .map(
                        autor -> {
                            autor.setNombre(newAutor.getNombre());
                            return autorRepository.save(autor);
                        }
                ).get();
    }
}
