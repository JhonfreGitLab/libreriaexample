package com.jhonfre.libreria.libreria.service;

import com.jhonfre.libreria.libreria.entity.Usuario;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UsuarioService {
    List<Usuario> get();
    Usuario createUsuario(Long idUser, String nombreUsuario, Long idTipoUsuario);
    void delete(Long id);
    Optional<Usuario> findById(Long id);
    Usuario updateUsuario(Long idUser, String nombreUsuario, Long idTipoUsuario);
}
